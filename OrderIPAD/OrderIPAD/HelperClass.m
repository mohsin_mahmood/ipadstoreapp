//
//  HelperClass.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "HelperClass.h"

@implementation HelperClass

-(void)showAlertViewtitle:(NSString*)title text:(NSString*)message Ctrl:(UIViewController*)controller
{

UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];

[alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
    [self closeAlertview];
}]];
    ctrl = controller;
dispatch_async(dispatch_get_main_queue(), ^ {
    [controller presentViewController:alertController animated:YES completion:nil];
});
}


-(void)closeAlertview
{
    [ctrl dismissViewControllerAnimated:YES completion:nil];
    ctrl = nil;
}

+(void)saveLoginSessionKey:(NSString*)string
{
    [[NSUserDefaults standardUserDefaults] setObject:string forKey:@"logginSession"];
    [[NSUserDefaults standardUserDefaults] synchronize ];
}

+(NSString*)getLoginSessionKey
{
   return [[NSUserDefaults standardUserDefaults] objectForKey:@"logginSession"];
}





@end
