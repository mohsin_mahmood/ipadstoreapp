//
//  ProductsParser.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/26/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "ProductsParser.h"
#import "CategoryModel.h"
@implementation ProductsParser

-(void)parseResponse:(NSDictionary *)dict
{
    [super parseResponse:dict];
    
    NSArray * data = [dict objectForKey:@"Data"];
    NSMutableArray * array = [[NSMutableArray alloc] init];
    for (NSDictionary * cat in data)
    {
        CategoryModel *model = [[CategoryModel alloc] init];
        [model populateCategoryModel:cat];
        [array addObject:model];
    }
    self.categories = array;
}

@end
