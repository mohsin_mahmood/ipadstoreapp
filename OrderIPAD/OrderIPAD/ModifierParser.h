//
//  ModifierParser.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/16/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import "ResponseParser.h"

@interface ModifierParser : ResponseParser
@property (nonatomic,strong) NSMutableArray * parsedModifiers;
@end
