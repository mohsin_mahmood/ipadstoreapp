//
//  HotelsViewController.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "HotelsViewController.h"
#import "MBProgressHUD.h"
#import "HelperClass.h"
#import "AFNetworking/AFNetworking.h"
#import "RestaurantModel.h"
#import "RestaurantParser.h"
#import "RestaurantCollectionViewCell.h"
#import "ProductListingViewController.h"
@interface HotelsViewController ()
{
    MBProgressHUD * layer;
}

@end

@implementation HotelsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    
    //doing service call for restaurants
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[[NSURL alloc] initWithString:[NSString stringWithFormat:@"http://datolee.com/API/QuickServices/GetRestaurantList?LoginSessionKey=%@",[HelperClass getLoginSessionKey]]]];
    request.timeoutInterval = 10;
    [request setHTTPMethod:@"GET"];
    
    AFHTTPRequestOperation * operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    layer = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [layer setLabelText:@"Loading Restaurants...."];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString * str  = [operation responseString];
        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        RestaurantParser * parser =[[RestaurantParser alloc ] init];
        [parser parseResponse:json];
        [layer hide:YES];
        if(parser.repsonseCode == 200)
        {
            restModels = parser.restaurants;
            if(restModels.count == 0)
            {
                [[[HelperClass alloc] init] showAlertViewtitle:@"" text:@"No Restaurants Available" Ctrl:self ];
//                //temp code
//                RestaurantModel * model = [[RestaurantModel alloc] init];
//                model.GUID = @"071be6c3-2223-4476-b760-d0ee7540a045";
//                model.imageURL = @"http://www.gettyimages.ca/gi-resources/images/Homepage/Hero/UK/CMS_Creative_164657191_Kingfisher.jpg";
//                
//                [restModels addObject:model];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionView reloadData];
            });
        }
        else
        {
            [[[HelperClass alloc] init] showAlertViewtitle:@"" text:parser.message Ctrl:self ];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [layer hide:YES];
        NSLog(@"error: %@", [operation error]);
        [[[HelperClass alloc] init] showAlertViewtitle:@"Error" text:@"some thing goes wrong. please check internet connection" Ctrl:self ];
    }];
    [operation start];
}

#pragma-mark Collection View Delegates

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if(restModels == nil)
    {
        return 0;
    }
    return 1;//restModels.count;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RestaurantCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"restaurantCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor whiteColor];
    [cell populateWithModel:[restModels objectAtIndex:indexPath.row]];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    RestaurantModel * model = [restModels objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"listingSeg" sender:model];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    ProductListingViewController * vc = [segue destinationViewController];
    vc.GUID = ((RestaurantModel*)sender).GUID;
}

@end
