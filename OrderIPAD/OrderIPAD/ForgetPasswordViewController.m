//
//  ForgetPasswordViewController.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "ForgetPasswordViewController.h"
#import "AFNetworking/AFNetworking.h"
#import "HelperClass.h"
#import "LoginParser.h"

@interface ForgetPasswordViewController ()
{
    CGPoint originX;
}

@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
        [self.navigationController setNavigationBarHidden:NO];
}


- (IBAction)sendCodeTapped:(id)sender
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[[NSURL alloc] initWithString:[NSString stringWithFormat:@"http://datolee.com/API/QuickServices/GetForgotEmailPassCode?Email=%@",self.username.text ]]];
    request.timeoutInterval = 10;
    [request setHTTPMethod:@"GET"];
 
    AFHTTPRequestOperation * operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
       layer = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [layer setLabelText:@"Sending Code...."];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString * str  = [operation responseString];
        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        ResponseParser * parser =[[ResponseParser alloc ] init];
        [parser parseResponse:json];
        [layer hide:YES];
        if(parser.repsonseCode == 200)
        {
            
            [self performSegueWithIdentifier:@"enterCodeSeg" sender:self];
        }
        else
        {
            [[[HelperClass alloc] init] showAlertViewtitle:@"" text:parser.message Ctrl:self ];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [layer hide:YES];
        NSLog(@"error: %@", [operation error]);
        [[[HelperClass alloc] init] showAlertViewtitle:@"Error" text:@"some thing goes wrong. please check internet connection" Ctrl:self ];
    }];
    [operation start];
}
#pragma-mark UIField delegates

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    originX = self.view.center;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    self.view.center = originX;
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.view.center = CGPointMake(originX.x, originX.y - 100);
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.view.center = originX;
}

@end
