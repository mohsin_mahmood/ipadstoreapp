//
//  OrderObject.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/16/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoryItemModel.h"
#import "ItemModifierModel.h"

@interface OrderObject : NSObject
@property(nonatomic,strong) CategoryItemModel * foodItem;
@property(nonatomic,strong) ItemModifierModel * foodItemModifier;
-(double)getPrice;
@end
