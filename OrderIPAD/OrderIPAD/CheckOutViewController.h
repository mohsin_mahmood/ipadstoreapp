//
//  CheckOutViewController.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/17/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface CheckOutViewController : UIViewController <UITextFieldDelegate>
{
    MBProgressHUD * layer;
}
@property (strong, nonatomic) IBOutlet UITextField *paymentByCustomer;
@property (strong, nonatomic) IBOutlet UILabel *changeAmount;

@property (strong, nonatomic) IBOutlet UILabel *totalBill;
@property (strong, nonatomic) NSArray *orderArray;
- (IBAction)completeorderTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *calculatorContainer;
@end
