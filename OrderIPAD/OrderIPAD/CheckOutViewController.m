//
//  CheckOutViewController.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/17/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import "CheckOutViewController.h"
#import "OrderObject.h"
#import "AFNetworking/AFNetworking.h"
#import "HelperClass.h"
#import "ResponseParser.h"

@interface CheckOutViewController ()

@end

@implementation CheckOutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    self.totalBill.text = [NSString stringWithFormat:@"%.2f",[self getTotalPrice]];
}

-(double)getTotalPrice
{
    double sum = 0;
    for( OrderObject * obj in self.orderArray)
    {
        
        sum = sum + [obj getPrice];
        
    }
    return sum;
}

- (IBAction)completeorderTapped:(id)sender
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[[NSURL alloc] initWithString:@"http://datolee.com/API/QuickServices/InsertSellingDetail"]];
    request.timeoutInterval = 10;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    //Setting Request

    
    NSMutableDictionary * param =[[NSMutableDictionary alloc] init];
    [param setObject:[HelperClass getLoginSessionKey] forKey:@"LoginSessionKey"];
    [param setObject:[NSString stringWithFormat:@"%.2f",[self getTotalPrice]] forKey:@"FinalAmount"];
    [param setObject:[NSString stringWithFormat:@"1"] forKey:@"PaymentMasterId"];
  
    NSMutableArray* array = [[NSMutableArray alloc] init];
    
    for( OrderObject * obj in self.orderArray)
    {
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        [dict setObject:obj.foodItem.itemGUID forKey:@"FoodDetailGUID"];
        [dict setObject:[NSString stringWithFormat:@"%.2f",[obj getPrice]] forKey:@"Subtotal"];
        [dict setObject:[NSString stringWithFormat:@"1"] forKey:@"Quantity"];
        [array addObject:dict];
    }
    [param setObject:array forKey:@"SellingDetail"];
    //json format to send the data
    NSData * jsondata = [NSJSONSerialization dataWithJSONObject:param
                                                        options:NSJSONWritingPrettyPrinted
                                                          error:nil];
    
    [request setHTTPBody:jsondata];
    
    AFHTTPRequestOperation * operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    layer = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [layer setLabelText:@"Please Wait, Submiting Order...."];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString * str  = [operation responseString];
        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        ResponseParser * parser =[[ResponseParser alloc ] init];
        [parser parseResponse:json];
        [layer hide:YES];
        if(parser.repsonseCode == 200)
        {
            
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            [[[HelperClass alloc] init] showAlertViewtitle:@"" text:parser.message Ctrl:self ];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [layer hide:YES];
        NSLog(@"error: %@", [operation error]);
        [[[HelperClass alloc] init] showAlertViewtitle:@"Error" text:@"some thing goes wrong. please check internet connection" Ctrl:self ];
    }];
    [operation start];

}


#pragma mark text
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    self.changeAmount.text = [NSString stringWithFormat:@"%.2f", textField.text.doubleValue - [self getTotalPrice]];
    return YES;
}



@end
