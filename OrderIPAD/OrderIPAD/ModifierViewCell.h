//
//  ModifierViewCell.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/16/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemModifierModel.h"
@interface ModifierViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *priceAndName;
@property (strong, nonatomic) IBOutlet UIButton *selectButton;
- (IBAction)itemSelectedTapped:(id)sender;
-(void)populateCellFromModel:(ItemModifierModel*)model;
@end
