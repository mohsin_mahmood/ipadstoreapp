//
//  ForgetPasswordViewController.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface ForgetPasswordViewController : UIViewController<UITextFieldDelegate>
{
    MBProgressHUD * layer;
}
@property (strong, nonatomic) IBOutlet UITextField *username;
- (IBAction)sendCodeTapped:(id)sender;

@end
