//
//  ResponseParser.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResponseParser : NSObject


-(void)parseResponse:(NSDictionary*)dict;
@property (nonatomic,strong) NSString * message;
@property (nonatomic) int repsonseCode;

@end
