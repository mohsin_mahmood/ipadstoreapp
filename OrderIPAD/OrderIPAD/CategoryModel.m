//
//  CategoryModel.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/26/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "CategoryModel.h"
@implementation CategoryModel
-(void)populateCategoryModel:(NSDictionary*)dict
{
    self.name = [dict objectForKey:@"Name"];
    self.categoryGUID = [dict objectForKey:@"CetegoryGUID"];
    self.foodDetailList = [[NSMutableArray alloc] init];
    NSArray * items = [dict objectForKey:@"FoodDetailList"];
    for (NSDictionary * catItem in items)
    {
        CategoryItemModel * model = [[CategoryItemModel alloc] init];
        [model populateCategoryItemModel:catItem];
        [self.foodDetailList addObject:model];
    }
}



@end
