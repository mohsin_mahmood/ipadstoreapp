//
//  RestaurantModel.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RestaurantModel : NSObject
@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) NSString* imageURL;
@property (nonatomic,strong) NSString* GUID;
-(void)populateRestaurant:(NSDictionary*)dict;

@end
