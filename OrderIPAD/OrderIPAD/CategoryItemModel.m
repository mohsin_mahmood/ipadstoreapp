//
//  CategoryItemModel.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/26/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "CategoryItemModel.h"

@implementation CategoryItemModel
-(void)populateCategoryItemModel:(NSDictionary*)dict
{
    self.foodName = [dict objectForKey:@"FoodName"];
    self.imageURL = [dict objectForKey:@"GeneratedFileName"];
    self.itemGUID = [dict objectForKey:@"FoodDetailGUID"];
    self.price = [[dict objectForKey:@"Price"] doubleValue];
}
@end
