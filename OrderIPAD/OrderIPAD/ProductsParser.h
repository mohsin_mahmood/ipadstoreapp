//
//  ProductsParser.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/26/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "ResponseParser.h"

@interface ProductsParser : ResponseParser
@property (nonatomic,strong) NSArray * categories;
@end
