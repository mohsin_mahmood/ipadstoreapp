//
//  productCollectionViewCell.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/11/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryItemModel.h"

@interface productCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UILabel *prodName;
-(void)populateWithModel:(CategoryItemModel *)model;
@end
