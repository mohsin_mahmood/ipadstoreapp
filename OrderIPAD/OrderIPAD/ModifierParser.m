//
//  ModifierParser.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/16/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import "ModifierParser.h"
#import "ItemModifierModel.h"

@implementation ModifierParser
-(void)parseResponse:(NSDictionary *)dict
{
    [super parseResponse:dict];
    self.parsedModifiers = [[NSMutableArray alloc] init];
    NSArray * data = [dict objectForKey:@"Data"];
    for (NSDictionary * mod in data)
    {
       NSArray * modifiers = [mod objectForKey:@"ModifierList"];
        for (NSDictionary * modifier in modifiers)
        {
            ItemModifierModel *model = [[ItemModifierModel alloc] init];
            [model populateItemModifier:modifier];
            [self.parsedModifiers addObject:model];
        }
    }
}
@end
