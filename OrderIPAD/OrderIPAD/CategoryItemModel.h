//
//  CategoryItemModel.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/26/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryItemModel : NSObject
@property (nonatomic,strong) NSString * foodName;
@property (nonatomic,strong) NSString * imageURL;
@property (nonatomic,strong) NSString * itemGUID;
@property (nonatomic) double price;
-(void)populateCategoryItemModel:(NSDictionary*)dict;

@end
