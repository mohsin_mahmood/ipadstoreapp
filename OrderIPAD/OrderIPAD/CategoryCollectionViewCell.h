//
//  CategoryCollectionViewCell.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/11/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryModel.h"

@interface CategoryCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UILabel *catName;
-(void)populateWithModel:(CategoryModel *)model;
@end
