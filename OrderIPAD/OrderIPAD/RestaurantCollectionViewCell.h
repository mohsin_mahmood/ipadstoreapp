//
//  RestaurantCollectionViewCell.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantModel.h"
@interface RestaurantCollectionViewCell : UICollectionViewCell
{
 
}
@property (strong, nonatomic) IBOutlet UIImageView *restaurantImage;
-(void)populateWithModel:(RestaurantModel *)model;

@end


//