//
//  ModifierViewCell.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/16/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import "ModifierViewCell.h"


@implementation ModifierViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    self.selectButton.selected = selected;
    // Configure the view for the selected state
}

- (IBAction)itemSelectedTapped:(id)sender
{
    UIButton * send = sender;
    send.selected = !send.selected;
}

-(void)populateCellFromModel:(ItemModifierModel *)model
{
    self.priceAndName.text = [NSString stringWithFormat:@"%@-%.2f",model.name,model.price];
}


@end
