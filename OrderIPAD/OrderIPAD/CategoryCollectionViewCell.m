//
//  CategoryCollectionViewCell.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/11/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import "CategoryCollectionViewCell.h"

@implementation CategoryCollectionViewCell
-(void)populateWithModel:(CategoryModel *)model
{
    self.catName.text = model.name;
}
-(void)setSelected:(BOOL)selected
{
    if(selected)
    {
    self.backgroundColor = [UIColor colorWithRed:(CGFloat)8/255 green:(CGFloat)117/255 blue:(CGFloat)164/255 alpha:1];
    self.catName.textColor = [UIColor whiteColor];
    }
    else
    {
        self.backgroundColor = [UIColor whiteColor];
        self.catName.textColor = [UIColor blackColor];
    }
}

@end
