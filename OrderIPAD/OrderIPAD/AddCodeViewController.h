//
//  AddCodeViewController.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
@interface AddCodeViewController : UIViewController<UITextFieldDelegate>
{
    MBProgressHUD * layer;
}
@property (strong, nonatomic) IBOutlet UITextField *username;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *passCode;
- (IBAction)resetTapped:(id)sender;

@end
