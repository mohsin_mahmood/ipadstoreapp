//
//  RestaurantCollectionViewCell.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "RestaurantCollectionViewCell.h"
#import "SDWebImage/SDImageCache.h"
#import "UIImageView+WebCache.h"
@implementation RestaurantCollectionViewCell


-(void)populateWithModel:(RestaurantModel *)model
{
    [self.restaurantImage sd_setImageWithURL:[NSURL URLWithString:model.imageURL ]
                                                        placeholderImage:nil];
}


@end
