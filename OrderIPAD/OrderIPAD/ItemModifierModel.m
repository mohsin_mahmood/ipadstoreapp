//
//  ItemModifierModel.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/16/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import "ItemModifierModel.h"

@implementation ItemModifierModel
-(void)populateItemModifier:(NSDictionary *)dict
{
    self.name = [dict objectForKey:@"Name"];
    self.price = ((NSNumber*)[dict objectForKey:@"Price"]).doubleValue;
}
@end
