//
//  HelperClass.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelperClass : NSObject
{
    UIViewController * ctrl;
}
-(void)showAlertViewtitle:(NSString*)title text:(NSString*)message Ctrl:(UIViewController*)controller;
+(void)saveLoginSessionKey:(NSString*)string;
+(NSString*)getLoginSessionKey;
@end
