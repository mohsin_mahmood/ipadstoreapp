//
//  RestaurantParser.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "RestaurantParser.h"
#import "RestaurantModel.h"

@implementation RestaurantParser

-(void)parseResponse:(NSDictionary *)dict
{
    [super parseResponse:dict];
    if([dict objectForKey:@"Data"])
    {
        NSArray * data = [dict objectForKey:@"Data"];
        NSMutableArray * array = [[NSMutableArray alloc] init];
        for (NSDictionary * dict in data)
        {
            RestaurantModel *model = [[RestaurantModel alloc] init];
            [model populateRestaurant:dict];
            [array addObject:model];
        }
        self.restaurants = array;
    }

}


@end
