//
//  HotelsViewController.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelsViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    NSMutableArray * restModels;
}
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end
