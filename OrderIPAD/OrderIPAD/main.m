//
//  main.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/5/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
