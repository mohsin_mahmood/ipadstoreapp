//
//  productCollectionViewCell.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/11/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import "productCollectionViewCell.h"
#import "UIImageView+WebCache.h"

@implementation productCollectionViewCell
-(void)populateWithModel:(CategoryItemModel *)model
{
    [self.productImage sd_setImageWithURL:[NSURL URLWithString:model.imageURL ]
                            placeholderImage:[UIImage imageNamed:@"macimage.png"]];
    self.prodName.text = model.foodName;
}



@end
