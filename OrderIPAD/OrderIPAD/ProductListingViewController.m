//
//  ProductListingViewController.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/26/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "ProductListingViewController.h"
#import "MBProgressHUD.h"
#import "HelperClass.h"
#import "AFNetworking/AFNetworking.h"
#import "ProductsParser.h"
#import "CategoryModel.h"
#import "productCollectionViewCell.h"
#import "CategoryCollectionViewCell.h"
#import "ItemModifierModel.h"
#import "ModifierParser.h"
#import "ModifierViewCell.h"
#import "OrderItemsTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "CheckOutViewController.h"


@interface ProductListingViewController ()
{
    MBProgressHUD * layer;
    NSArray * catModels;
    CategoryModel * selectedModel;
    CategoryItemModel * selectedItem;
    ItemModifierModel * selectedModifier;
    NSMutableArray * orderArray;
}

@end

@implementation ProductListingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.modifiersArray = [[NSArray alloc] init];
    orderArray = [[NSMutableArray alloc] init];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeItemFromOrder:) name:@"itemDeleted" object:nil];
    // Do any additional setup after loading the view.
}

-(void)removeItemFromOrder:(NSNotification*)notif
{
    OrderObject * obj = notif.object;
    [orderArray removeObject:obj];
    [self showOrderITemView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self closeAllpopUps:nil];
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    CALayer * layer2 = self.amountlabelBG.layer;
    layer2.cornerRadius = 16;
    layer2.masksToBounds = YES;
    
    //doing service call for restaurants
    NSString * urlString = [NSString stringWithFormat:@"http://datolee.com/API/QuickServices/GetRestaurentFoodDetailList?RestaurentGUID=%@&LoginSessionKey=%@",self.GUID,[HelperClass getLoginSessionKey]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[[NSURL alloc] initWithString:urlString]];
    request.timeoutInterval = 10;
    [request setHTTPMethod:@"GET"];
    
    AFHTTPRequestOperation * operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    layer = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [layer setLabelText:@"Loading Products...."];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString * str  = [operation responseString];
        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        ProductsParser * parser =[[ProductsParser alloc ] init];
        [parser parseResponse:json];
        [layer hide:YES];
        if(parser.repsonseCode == 200)
        {
            catModels = parser.categories;
            if(catModels.count == 0)
            {
                [[[HelperClass alloc] init] showAlertViewtitle:@"" text:@"No items Available" Ctrl:self ];
        
            }
            selectedModel = (CategoryModel*)[catModels objectAtIndex:0];
            [self.productsCollectionView reloadData];
            [self.categoryCollectionView reloadData];
        }
        else
        {
            [[[HelperClass alloc] init] showAlertViewtitle:@"" text:parser.message Ctrl:self ];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [layer hide:YES];
        NSLog(@"error: %@", [operation error]);
        [[[HelperClass alloc] init] showAlertViewtitle:@"Error" text:@"some thing goes wrong. please check internet connection" Ctrl:self ];
    }];
    [operation start];
}

- (IBAction)showBill:(id)sender
{
    [self showOrderITemView];
}

//MARK:
#pragma-mark Collection View Delegates

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    if(catModels == nil)
    {
        return 0;
    }
    if(view == self.productsCollectionView)
    {
        return selectedModel.foodDetailList.count;
    }
    if(view == self.categoryCollectionView)
    {
        return catModels.count;
    }
    return 0;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell * cellfinal;
    if(cv == self.productsCollectionView)
    {
        productCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"prodCell" forIndexPath:indexPath];
        [cell populateWithModel:[selectedModel.foodDetailList objectAtIndex:indexPath.row]];
        cellfinal = cell;
    }
    else if(cv == self.categoryCollectionView)
    {
        CategoryCollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"catCell" forIndexPath:indexPath];
        [cell populateWithModel:[catModels objectAtIndex:indexPath.row]];
        if ([catModels objectAtIndex:indexPath.row]==selectedModel) {
            cell.selected = YES;
            [cv scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:catModels.count-1 inSection:0] atScrollPosition:UICollectionViewScrollPositionRight animated:YES];
        }
        cellfinal = cell;
    }
    
    return cellfinal;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.categoryCollectionView == collectionView)
    {
        selectedModel = [catModels objectAtIndex:indexPath.row];
        [self.productsCollectionView reloadData];
    }
    else if(self.productsCollectionView == collectionView)
    {
        selectedItem = [selectedModel.foodDetailList objectAtIndex:indexPath.row];
        [self loadModifiersForSelectedItem];
    }
    
}

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)closeAllpopUps:(id)sender
{
    self.modifierView.hidden = YES;
    self.orderItemCheckOutView.hidden = YES;
    self.screenModifierView.hidden = YES;
}

-(void)showModifierView
{
    self.modifierView.hidden = NO;
    self.orderItemCheckOutView.hidden = YES;
    self.screenModifierView.hidden = NO;
    selectedModifier = nil;
    [self.productImage sd_setImageWithURL:[NSURL URLWithString:selectedItem.imageURL]
                                             placeholderImage:[UIImage imageNamed:@"macimage.png"]];
    self.productNAme.text = selectedItem.foodName;
    self.price.text = [NSString stringWithFormat:@"$%.2f", selectedItem.price];
    self.quantity.text = 0;
    self.totalPrice.text = [NSString stringWithFormat:@"$%.2f", selectedItem.price];
    
}

-(void)showOrderITemView
{
    self.modifierView.hidden = YES;
    self.orderItemCheckOutView.hidden = NO;
    self.screenModifierView.hidden = NO;
    self.priceLabel.text = [NSString stringWithFormat:@"$%.2f",[self getTotalPrice]];

    [self.orderItemTableview reloadData];
}

-(double)getTotalPrice
{
    double sum = 0;
    for( OrderObject * obj in orderArray)
    {
        if(obj.foodItemModifier != nil)
        {
            sum = sum + obj.foodItemModifier.price;
        }
        else
        {
            sum = sum + obj.foodItem.price;
        }
    }
    return sum;
}


//Modifier PopUp Operation

-(void)loadModifiersForSelectedItem
{
    selectedModifier = nil;
    //doing service call for restaurants
    NSString * urlString = [NSString stringWithFormat:@"http://datolee.com/API/QuickServices/GetFoodModifierDetails?LoginSessionKey=%@&CetegoryGUID=%@&FoodDetailGUID=%@",[HelperClass getLoginSessionKey],selectedModel.categoryGUID,selectedItem.itemGUID];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[[NSURL alloc] initWithString:urlString]];
    request.timeoutInterval = 10;
    [request setHTTPMethod:@"GET"];
    
    AFHTTPRequestOperation * operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    layer = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [layer setLabelText:@"Loading Modifiers...."];
    self.modifierTableView.hidden = NO;
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString * str  = [operation responseString];
        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        ModifierParser * parser =[[ModifierParser alloc ] init];
        [parser parseResponse:json];
        [layer hide:YES];
        if(parser.repsonseCode == 200)
        {
            self.modifiersArray = parser.parsedModifiers;
            [self showModifierView];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.modifierTableView reloadData];
                if(self.modifiersArray.count == 0)
                {
                    self.modifierTableView.hidden = YES;
                }
            });
        }
        else
        {
            [[[HelperClass alloc] init] showAlertViewtitle:@"" text:parser.message Ctrl:self ];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [layer hide:YES];
        NSLog(@"error: %@", [operation error]);
        [[[HelperClass alloc] init] showAlertViewtitle:@"Error" text:@"some thing goes wrong. please check internet connection" Ctrl:self ];
    }];
    [operation start];
    
}

- (IBAction)addToOrderButtonTapped:(id)sender
{
    for (int x = 0 ; x < self.quantity.text.integerValue ; x++)
    {
        OrderObject * obj = [[OrderObject alloc] init];
        obj.foodItem = selectedItem;
        obj.foodItemModifier = selectedModifier;
        [orderArray addObject:obj];
    }
    [self closeAllpopUps:nil];
}
- (IBAction)proceedWithCheckOutPressed:(id)sender
{
    if(orderArray.count > 0)
    {
        [self performSegueWithIdentifier:@"checkOut" sender:nil];
    }
}

- (IBAction)deleteOrderPress:(id)sender
{
    [self closeAllpopUps:nil];
    [orderArray removeAllObjects];
}

#pragma mark TableView Delegates
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger abc = 0;
    if(self.modifierTableView == tableView)
    {
        abc = self.modifiersArray.count;
    }
    else
    {
        abc = orderArray.count;
    }
    return abc;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell * cell = nil;
    if(self.modifierTableView == tableView)
    {
        ModifierViewCell * cell1 =(ModifierViewCell*) [tableView dequeueReusableCellWithIdentifier:@"modCell"];
        [cell1 populateCellFromModel:[self.modifiersArray objectAtIndex:indexPath.row]];
        cell = cell1;
    }
    else
    {
         OrderItemsTableViewCell * cell1 =(OrderItemsTableViewCell*)  [tableView dequeueReusableCellWithIdentifier:@"orderCell"];
        [cell1 populateCellFromModel:[orderArray objectAtIndex:indexPath.row]];
        cell = cell1;
    }
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if(self.modifierTableView == tableView)
    {
        selectedModifier = [self.modifiersArray objectAtIndex:indexPath.row];
       self.totalPrice.text = [NSString stringWithFormat:@"$%.2f", selectedModifier.price*self.quantity.text.integerValue];
    
    }
}

//mark: delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(selectedModifier)
    {
        self.totalPrice.text = [NSString stringWithFormat:@"$%.2f", selectedModifier.price*self.quantity.text.integerValue];
    }
    else
    {
        self.totalPrice.text = [NSString stringWithFormat:@"$%.2f", selectedItem.price*self.quantity.text.integerValue];
    }
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    return YES;
}


#pragma mark navigation
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    CheckOutViewController * ctr = segue.destinationViewController;
    ctr.orderArray = orderArray;
}



@end
