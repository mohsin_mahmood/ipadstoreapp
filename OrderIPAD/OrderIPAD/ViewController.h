//
//  ViewController.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/5/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface ViewController : UIViewController<UITextFieldDelegate>

{
    MBProgressHUD * layer;
}
@property (strong, nonatomic) IBOutlet UITextField *username;

@property (strong, nonatomic) IBOutlet UITextField *password;
- (IBAction)loggin:(id)sender;

@end

