//
//  RestaurantParser.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "ResponseParser.h"

@interface RestaurantParser : ResponseParser
@property (nonatomic,strong) NSMutableArray * restaurants;


@end
