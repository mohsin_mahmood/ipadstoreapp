//
//  RestaurantModel.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "RestaurantModel.h"

@implementation RestaurantModel

-(void)populateRestaurant:(NSDictionary*)dict
{
    self.name = [dict objectForKey:@"RestaurentName"];
        self.imageURL = [dict objectForKey:@"Logo"];
        self.GUID = [dict objectForKey:@"RestaurentGUID"];
}


@end
