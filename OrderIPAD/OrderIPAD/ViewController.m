//
//  ViewController.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/5/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking/AFNetworking.h"
#import "HelperClass.h"
#import "LoginParser.h"

@interface ViewController ()
{
    CGPoint originX;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (IBAction)loggin:(id)sender
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[[NSURL alloc] initWithString:@"http://datolee.com/API/QuickServices/LoginAuthentication"]];
    request.timeoutInterval = 10;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    //Setting Request
    
    
    NSMutableDictionary * param =[[NSMutableDictionary alloc] init];
    [param setObject:self.username.text forKey:@"Email"];
    [param setObject:self.password.text forKey:@"Password"];
    
    //json format to send the data
    NSData * jsondata = [NSJSONSerialization dataWithJSONObject:param
                                                        options:NSJSONWritingPrettyPrinted
                                                          error:nil];
    
    [request setHTTPBody:jsondata];
    
    
    AFHTTPRequestOperation * operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    layer = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [layer setLabelText:@"Log in...."];

    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString * str  = [operation responseString];
        NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary * json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        LoginParser * parser =[[LoginParser alloc ] init];
        [parser parseResponse:json];
        [layer hide:YES];
       if(parser.repsonseCode == 200)
       {
           [self performSegueWithIdentifier:@"loginSeg" sender:self];
       }
        else
        {
            [[[HelperClass alloc] init] showAlertViewtitle:@"" text:parser.message Ctrl:self ];
        }
 
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error: %@", [operation error]);
        [layer hide:YES];
        [[[HelperClass alloc] init] showAlertViewtitle:@"Error" text:@"some thing goes wrong. please check internet connection" Ctrl:self ];
    }];
    [operation start];

}

#pragma-mark UIField delegates

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    originX = self.view.center;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    self.view.center = originX;
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.view.center = CGPointMake(originX.x, originX.y - 100);
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    self.view.center = originX;
}



@end
