//
//  ItemModifierModel.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/16/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ItemModifierModel : NSObject
@property(nonatomic,strong) NSString * name;
@property(nonatomic) double price;
-(void)populateItemModifier:(NSDictionary*)dict;
@end
