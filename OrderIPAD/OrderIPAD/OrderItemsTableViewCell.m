//
//  OrderItemsTableViewCell.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/16/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import "OrderItemsTableViewCell.h"
#import "UIImageView+WebCache.h"

@implementation OrderItemsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
    // Configure the view for the selected state
}

- (IBAction)deleteItemClicked:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"itemDeleted" object:self.obj];
}

-(void)populateCellFromModel:(OrderObject *)model
{
    self.price.text = [NSString stringWithFormat:@"%.2f",model.getPrice];
    self.totalPrice.text = [NSString stringWithFormat:@"%.2f",model.getPrice];
    if(model.foodItemModifier != nil)
        self.modifier.text = model.foodItemModifier.name;
    [self.item sd_setImageWithURL:[NSURL URLWithString:model.foodItem.imageURL]
                         placeholderImage:[UIImage imageNamed:@"macimage.png"]];
    self.name.text = model.foodItem.foodName;
    self.obj = model;
}

@end
