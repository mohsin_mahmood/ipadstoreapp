//
//  CategoryModel.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/26/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CategoryItemModel.h"

@interface CategoryModel : NSObject
@property (nonatomic,strong) NSString * categoryGUID;
@property (nonatomic,strong) NSString * name;
@property (nonatomic,strong) NSMutableArray * foodDetailList;
-(void)populateCategoryModel:(NSDictionary*)dict;
@end
