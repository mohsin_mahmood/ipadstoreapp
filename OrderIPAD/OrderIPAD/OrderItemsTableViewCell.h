//
//  OrderItemsTableViewCell.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/16/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderObject.h"
@interface OrderItemsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UILabel *totalPrice;
@property (strong, nonatomic) IBOutlet UILabel *modifier;
@property (strong, nonatomic) IBOutlet UIImageView *item;
@property (strong, nonatomic) IBOutlet UILabel *name;
@property (strong, nonatomic) OrderObject *obj;
- (IBAction)deleteItemClicked:(id)sender;
-(void)populateCellFromModel:(OrderObject*)model;
@end
