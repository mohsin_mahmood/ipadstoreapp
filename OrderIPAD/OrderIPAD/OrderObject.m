//
//  OrderObject.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 1/16/16.
//  Copyright © 2016 Mohsin Mahmood. All rights reserved.
//

#import "OrderObject.h"

@implementation OrderObject
-(double)getPrice
{
    double sum = 0;
    if(self.foodItemModifier != nil)
    {
        sum = self.foodItemModifier.price;
    }
    else
    {
        sum =  self.foodItem.price;
    }

    return sum;
}

@end
