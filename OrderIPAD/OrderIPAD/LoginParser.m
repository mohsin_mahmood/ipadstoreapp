//
//  LoginParser.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "LoginParser.h"

@implementation LoginParser
-(void)parseResponse:(NSDictionary *)dict
{
    [super parseResponse:dict];
    if([dict objectForKey:@"Data"])
    {
        NSDictionary * dataDict = [dict objectForKey:@"Data"];
        
        self.firstName = [dataDict objectForKey:@"FirstName"];
        self.lastName = [dataDict objectForKey:@"LastName"];
    }
}
@end
