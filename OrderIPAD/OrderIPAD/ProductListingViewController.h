//
//  ProductListingViewController.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/26/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductListingViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UITableViewDataSource,UITableViewDataSource,UITextFieldDelegate>
@property (nonatomic,strong) NSString * GUID;
@property (strong, nonatomic) IBOutlet UICollectionView *productsCollectionView;
- (IBAction)showBill:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *amountlabelBG;
@property (strong, nonatomic) IBOutlet UICollectionView *categoryCollectionView;
- (IBAction)backButtonTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *screenModifierView;
@property (strong, nonatomic) IBOutlet UIView *modifierView;
@property (strong, nonatomic) IBOutlet UIView *orderItemCheckOutView;
//modifier block image thngs
@property (strong,nonatomic) NSArray * modifiersArray;
@property (strong, nonatomic) IBOutlet UIImageView *productImage;
@property (strong, nonatomic) IBOutlet UILabel *productNAme;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UITextField *quantity;

@property (strong, nonatomic) IBOutlet UITableView *modifierTableView;
@property (strong, nonatomic) IBOutlet UILabel *totalPrice;
- (IBAction)addToOrderButtonTapped:(id)sender;

//Order Verify and CheckOut Screen

@property (strong, nonatomic) IBOutlet UITableView *orderItemTableview;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
- (IBAction)proceedWithCheckOutPressed:(id)sender;
- (IBAction)deleteOrderPress:(id)sender;

@end
