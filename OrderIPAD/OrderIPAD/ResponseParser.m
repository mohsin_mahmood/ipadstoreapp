//
//  ResponseParser.m
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import "ResponseParser.h"
#import "HelperClass.h"

@implementation ResponseParser
-(void)parseResponse:(NSDictionary *)dict
{
    self.message = [dict objectForKey:@"Message"];
    self.repsonseCode = [[dict objectForKey:@"ResponseCode"] intValue];
    if([dict objectForKey:@"Data"])
    {
        if([[dict objectForKey:@"Data"] isKindOfClass:[NSDictionary class]])
        {
            if([[dict objectForKey:@"Data"] objectForKey:@"LoginSessionKey"])
            {
                [HelperClass saveLoginSessionKey:[[dict objectForKey:@"Data"] objectForKey:@"LoginSessionKey"]];
            }
        }
    }
}

@end
