//
//  LoginParser.h
//  OrderIPAD
//
//  Created by Mohsin Mahmood on 12/25/15.
//  Copyright © 2015 Mohsin Mahmood. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResponseParser.h"

@interface LoginParser : ResponseParser

@property (nonatomic,strong) NSString* firstName;
@property (nonatomic,strong) NSString* lastName;

-(void)parseResponse:(NSDictionary *)dict;


@end
